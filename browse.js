// Puppeteer script to take screenshots of all 2013-15 application histories of Gunn students toward colleges
// 541 colleges
// 3/4/2018
// Bryan Owens

// A tutorial of Puppeteer can be found at https://github.com/emadehsan/thal

const puppeteer = require('puppeteer');

// Get username and password
const CREDS = require('./creds');

// Selectors
const USERNAME_SELECTOR = '#login > fieldset > ul > li:nth-child(1) > input[type="text"]';
const PASSWORD_SELECTOR = '#login > fieldset > ul > li:nth-child(2) > input[type="password"]';
const LOGIN_SELECTOR = '#login > fieldset > ul > li:nth-child(4) > input';
const COLLEGES_BTN_SELECTOR = '#nav > li:nth-child(2) > a';

async function run() {
	const browser = await puppeteer.launch({
		headless: true, // toggle to false for debugging
		ignoreHTTPSErrors: true
	});

	const page = await browser.newPage().catch();

	// Set viewport options
	await page.setViewport({
		height: 1200,
		width: 1400,
		// larger integer for deviceScaleFactor increases 
		// quality of text in resulting screenshot
		deviceScaleFactor: 2, 
		isMobile: false
	}).catch();

	// Navigate to Naviance website
	await page.goto('https://connection.naviance.com/family-connection/auth/login?hsid=hmghs').catch();
	


	// Login
	await page.click(USERNAME_SELECTOR).catch();
	await page.keyboard.type(CREDS.username).catch();
	await page.click(PASSWORD_SELECTOR).catch();
	await page.keyboard.type(CREDS.password).catch();
	await page.click(LOGIN_SELECTOR).catch();
	await page.waitForNavigation().catch();

	// Go to college acceptance history list
	await page.goto('https://connection.naviance.com/family-connection/colleges/acceptance').catch();

	var numColleges;
	var collegeURLs;
	var collegeNames;

	// Navigate to each page and take a screenshot
	var numColleges = await page.evaluate(
		() => {
			let data = [];
			numColleges = document.querySelectorAll('#main-content > table > tbody > tr').length;
			return numColleges;
		}
	).catch();

	var i = 0;
	for (i = 1; i <= numColleges; i++) {
		const page2 = await browser.newPage().catch();

		await page2.setViewport({
			height: 1200,
			width: 1200,
			// larger integer for deviceScaleFactor increases 
			// quality of text in resulting screenshot
			deviceScaleFactor: 2, 
			isMobile: false
		}).catch();

		await page2.goto('https://connection.naviance.com/family-connection/colleges/acceptance', {waituntil: 'networkidle0'}).catch();

		let collegeLandingSelector = '#main-content > table > tbody > tr:nth-child(' + i + ') > td:nth-child(1) > a';
		let collegeNameSelector = collegeLandingSelector;
		let collegeAdmissionsSelector = '#surveyIntroScrollAnchor > div.ng-isolate-scope > div > div > div > div > div > div > span:nth-child(4)';
		let annoyingPopupX = '#hubs-intro-anchor-fav > div > div > div > div:nth-child(1) > div > div.masthead__right.fc-grid__col.fc-grid__col--xs-12.fc-grid__col--sm-8 > div.masthead__apply-to-container.ng-scope > div > div > span.hub-tooltip__close > svg';
		let ap2 =  '#surveyIntroScrollAnchor > div.ng-isolate-scope > div > div > div > div > div > div > span.hubs-top-tabs.ng-binding.ng-scope.hubs-top-tabs--active > div > span.hub-tooltip__close > svg';
		let ap3 = '#hubs-intro-anchor-fav > div > div > div > div:nth-child(1) > div > div.masthead__right.fc-grid__col.fc-grid__col--xs-12.fc-grid__col--sm-8 > div.masthead__apply-to-container.ng-scope > div > div > span.hub-tooltip__close > svg';
		let ap4 = '#hubs-intro-anchor-feedback > div > span.hub-tooltip__close > svg';
	
		// console.log("collegeAdmissionsSelector (#1): " + collegeAdmissionsSelector);

		let collegeName = await page.evaluate(
			(i, collegeNameSelector) => {
				return document.querySelector(collegeNameSelector).innerHTML;
			}, i, collegeNameSelector
		).catch();

		console.log("Navigating to #" + i + ": " + collegeName + "...");

/*
		const [response] = await Promise.all([
			page.click(collegeLandingSelector),
			page.waitForNavigation(),
			console.log("Navigating to admissions page...")
		]);
*/
		await page2.click(collegeLandingSelector).catch(); // https://github.com/GoogleChrome/puppeteer/issues/1412

		console.log("Navigating to admissions page...");
		await page2.waitFor(3000).catch();
		/*
		let admissionsURL = await page2.evaluate(
			() => {
				let url = window.location.href;
				console.log(url);
				console.log(url.substring(0, url.length-7).concat("Admissions"));
				return url.substring(0, url.length-7).concat("Admissions");
			}
		);
		*/

		let admissionsURL = await page2.evaluate(
			() => {
				let loc = window.location.href;
				return loc.substring(0, loc.length - 8).concat("Admissions");
			}
		).catch();
		console.log("Navigating to page " + admissionsURL + " ...");
		page2.goto(admissionsURL).catch();
		await page2.waitFor(3000).catch();
		page2.click(annoyingPopupX).catch();
		page2.click(ap2).catch();
		page2.click(ap3).catch();
		page2.click(ap4).catch();
		await page2.waitFor(1000).catch();

		page.evaluate(
			() => {
  				window.scrollBy(0, window.innerHeight);
			}
		).catch();

/*
		const [response] = await Promise.all([
			page2.goto(admissionsURL),
			page.waitForNavigation(),
			console.log("Navigating to admissions page...")
		]);
*/
		// await page2.waitFor(2000);
		console.log("Taking a screenshot...");

		// Take screenshot
		await page2.screenshot({
			fullPage: true, 
			path: "screens/" + collegeName + " (2015-2017).png"
		}).catch();
		console.log("Successfully took screenshot.");
		await page2.close().catch();
	}

	// Terminate process
	console.log("Successfully took screenshots of " + i + " colleges. Exiting.");

	// Delete this new tab from memory, otherwise risk overflowing memory
	await browser.close();
}

run();